WPGgallery = new Object();
WPGgallery.name = "";
WPGgallery.photographer = "";
WPGgallery.contact = "";
WPGgallery.email = "";
WPGgallery.date = ""

WPGgallery.colors = new Object();
WPGgallery.colors.background = "#FFFFFF";
WPGgallery.colors.banner = "#F0F0F0";
WPGgallery.colors.text = "#000000";
WPGgallery.colors.link = "#0000FF";
WPGgallery.colors.alink = "#FF0000";
WPGgallery.colors.vlink = "#800080";

gPhotos = new Array();
gPhotos[0] = new Object();
gPhotos[0].filename = "10.jpg";
gPhotos[0].ImageWidth = 480;
gPhotos[0].ImageHeight = 500;
gPhotos[0].ThumbWidth = 192;
gPhotos[0].ThumbHeight = 200;
gPhotos[0].meta = new Object();

gPhotos[1] = new Object();
gPhotos[1].filename = "11.jpg";
gPhotos[1].ImageWidth = 478;
gPhotos[1].ImageHeight = 500;
gPhotos[1].ThumbWidth = 191;
gPhotos[1].ThumbHeight = 200;
gPhotos[1].meta = new Object();

gPhotos[2] = new Object();
gPhotos[2].filename = "13.jpg";
gPhotos[2].ImageWidth = 500;
gPhotos[2].ImageHeight = 451;
gPhotos[2].ThumbWidth = 200;
gPhotos[2].ThumbHeight = 180;
gPhotos[2].meta = new Object();

gPhotos[3] = new Object();
gPhotos[3].filename = "14.jpg";
gPhotos[3].ImageWidth = 482;
gPhotos[3].ImageHeight = 500;
gPhotos[3].ThumbWidth = 192;
gPhotos[3].ThumbHeight = 200;
gPhotos[3].meta = new Object();

gPhotos[4] = new Object();
gPhotos[4].filename = "15.jpg";
gPhotos[4].ImageWidth = 481;
gPhotos[4].ImageHeight = 500;
gPhotos[4].ThumbWidth = 192;
gPhotos[4].ThumbHeight = 200;
gPhotos[4].meta = new Object();

gPhotos[5] = new Object();
gPhotos[5].filename = "16.jpg";
gPhotos[5].ImageWidth = 481;
gPhotos[5].ImageHeight = 500;
gPhotos[5].ThumbWidth = 192;
gPhotos[5].ThumbHeight = 200;
gPhotos[5].meta = new Object();

gPhotos[6] = new Object();
gPhotos[6].filename = "17.jpg";
gPhotos[6].ImageWidth = 500;
gPhotos[6].ImageHeight = 332;
gPhotos[6].ThumbWidth = 200;
gPhotos[6].ThumbHeight = 132;
gPhotos[6].meta = new Object();

gPhotos[7] = new Object();
gPhotos[7].filename = "18.jpg";
gPhotos[7].ImageWidth = 330;
gPhotos[7].ImageHeight = 500;
gPhotos[7].ThumbWidth = 132;
gPhotos[7].ThumbHeight = 200;
gPhotos[7].meta = new Object();

gPhotos[8] = new Object();
gPhotos[8].filename = "19.jpg";
gPhotos[8].ImageWidth = 500;
gPhotos[8].ImageHeight = 334;
gPhotos[8].ThumbWidth = 200;
gPhotos[8].ThumbHeight = 133;
gPhotos[8].meta = new Object();

gPhotos[9] = new Object();
gPhotos[9].filename = "2.jpg";
gPhotos[9].ImageWidth = 500;
gPhotos[9].ImageHeight = 338;
gPhotos[9].ThumbWidth = 200;
gPhotos[9].ThumbHeight = 135;
gPhotos[9].meta = new Object();

gPhotos[10] = new Object();
gPhotos[10].filename = "20.jpg";
gPhotos[10].ImageWidth = 500;
gPhotos[10].ImageHeight = 333;
gPhotos[10].ThumbWidth = 200;
gPhotos[10].ThumbHeight = 133;
gPhotos[10].meta = new Object();

gPhotos[11] = new Object();
gPhotos[11].filename = "21.jpg";
gPhotos[11].ImageWidth = 335;
gPhotos[11].ImageHeight = 500;
gPhotos[11].ThumbWidth = 134;
gPhotos[11].ThumbHeight = 200;
gPhotos[11].meta = new Object();

gPhotos[12] = new Object();
gPhotos[12].filename = "25.jpg";
gPhotos[12].ImageWidth = 347;
gPhotos[12].ImageHeight = 500;
gPhotos[12].ThumbWidth = 138;
gPhotos[12].ThumbHeight = 200;
gPhotos[12].meta = new Object();

gPhotos[13] = new Object();
gPhotos[13].filename = "27.jpg";
gPhotos[13].ImageWidth = 500;
gPhotos[13].ImageHeight = 337;
gPhotos[13].ThumbWidth = 200;
gPhotos[13].ThumbHeight = 134;
gPhotos[13].meta = new Object();

gPhotos[14] = new Object();
gPhotos[14].filename = "28.jpg";
gPhotos[14].ImageWidth = 332;
gPhotos[14].ImageHeight = 500;
gPhotos[14].ThumbWidth = 133;
gPhotos[14].ThumbHeight = 200;
gPhotos[14].meta = new Object();

gPhotos[15] = new Object();
gPhotos[15].filename = "3.jpg";
gPhotos[15].ImageWidth = 356;
gPhotos[15].ImageHeight = 500;
gPhotos[15].ThumbWidth = 142;
gPhotos[15].ThumbHeight = 200;
gPhotos[15].meta = new Object();

gPhotos[16] = new Object();
gPhotos[16].filename = "30.jpg";
gPhotos[16].ImageWidth = 500;
gPhotos[16].ImageHeight = 333;
gPhotos[16].ThumbWidth = 200;
gPhotos[16].ThumbHeight = 133;
gPhotos[16].meta = new Object();

gPhotos[17] = new Object();
gPhotos[17].filename = "31.jpg";
gPhotos[17].ImageWidth = 352;
gPhotos[17].ImageHeight = 500;
gPhotos[17].ThumbWidth = 140;
gPhotos[17].ThumbHeight = 200;
gPhotos[17].meta = new Object();

gPhotos[18] = new Object();
gPhotos[18].filename = "32.jpg";
gPhotos[18].ImageWidth = 500;
gPhotos[18].ImageHeight = 360;
gPhotos[18].ThumbWidth = 200;
gPhotos[18].ThumbHeight = 144;
gPhotos[18].meta = new Object();

gPhotos[19] = new Object();
gPhotos[19].filename = "33.jpg";
gPhotos[19].ImageWidth = 500;
gPhotos[19].ImageHeight = 362;
gPhotos[19].ThumbWidth = 200;
gPhotos[19].ThumbHeight = 144;
gPhotos[19].meta = new Object();

gPhotos[20] = new Object();
gPhotos[20].filename = "35.jpg";
gPhotos[20].ImageWidth = 500;
gPhotos[20].ImageHeight = 340;
gPhotos[20].ThumbWidth = 200;
gPhotos[20].ThumbHeight = 136;
gPhotos[20].meta = new Object();

gPhotos[21] = new Object();
gPhotos[21].filename = "36.jpg";
gPhotos[21].ImageWidth = 500;
gPhotos[21].ImageHeight = 356;
gPhotos[21].ThumbWidth = 200;
gPhotos[21].ThumbHeight = 142;
gPhotos[21].meta = new Object();

gPhotos[22] = new Object();
gPhotos[22].filename = "37.jpg";
gPhotos[22].ImageWidth = 484;
gPhotos[22].ImageHeight = 500;
gPhotos[22].ThumbWidth = 193;
gPhotos[22].ThumbHeight = 200;
gPhotos[22].meta = new Object();

gPhotos[23] = new Object();
gPhotos[23].filename = "38.jpg";
gPhotos[23].ImageWidth = 480;
gPhotos[23].ImageHeight = 500;
gPhotos[23].ThumbWidth = 192;
gPhotos[23].ThumbHeight = 200;
gPhotos[23].meta = new Object();

gPhotos[24] = new Object();
gPhotos[24].filename = "39.jpg";
gPhotos[24].ImageWidth = 500;
gPhotos[24].ImageHeight = 337;
gPhotos[24].ThumbWidth = 200;
gPhotos[24].ThumbHeight = 135;
gPhotos[24].meta = new Object();

gPhotos[25] = new Object();
gPhotos[25].filename = "4.jpg";
gPhotos[25].ImageWidth = 358;
gPhotos[25].ImageHeight = 500;
gPhotos[25].ThumbWidth = 143;
gPhotos[25].ThumbHeight = 200;
gPhotos[25].meta = new Object();

gPhotos[26] = new Object();
gPhotos[26].filename = "40.jpg";
gPhotos[26].ImageWidth = 500;
gPhotos[26].ImageHeight = 346;
gPhotos[26].ThumbWidth = 200;
gPhotos[26].ThumbHeight = 138;
gPhotos[26].meta = new Object();

gPhotos[27] = new Object();
gPhotos[27].filename = "41.jpg";
gPhotos[27].ImageWidth = 339;
gPhotos[27].ImageHeight = 500;
gPhotos[27].ThumbWidth = 135;
gPhotos[27].ThumbHeight = 200;
gPhotos[27].meta = new Object();

gPhotos[28] = new Object();
gPhotos[28].filename = "42.jpg";
gPhotos[28].ImageWidth = 500;
gPhotos[28].ImageHeight = 338;
gPhotos[28].ThumbWidth = 200;
gPhotos[28].ThumbHeight = 135;
gPhotos[28].meta = new Object();

gPhotos[29] = new Object();
gPhotos[29].filename = "45.jpg";
gPhotos[29].ImageWidth = 500;
gPhotos[29].ImageHeight = 340;
gPhotos[29].ThumbWidth = 200;
gPhotos[29].ThumbHeight = 136;
gPhotos[29].meta = new Object();

gPhotos[30] = new Object();
gPhotos[30].filename = "46.jpg";
gPhotos[30].ImageWidth = 500;
gPhotos[30].ImageHeight = 335;
gPhotos[30].ThumbWidth = 200;
gPhotos[30].ThumbHeight = 134;
gPhotos[30].meta = new Object();

gPhotos[31] = new Object();
gPhotos[31].filename = "47.jpg";
gPhotos[31].ImageWidth = 500;
gPhotos[31].ImageHeight = 366;
gPhotos[31].ThumbWidth = 200;
gPhotos[31].ThumbHeight = 146;
gPhotos[31].meta = new Object();

gPhotos[32] = new Object();
gPhotos[32].filename = "48.jpg";
gPhotos[32].ImageWidth = 331;
gPhotos[32].ImageHeight = 500;
gPhotos[32].ThumbWidth = 132;
gPhotos[32].ThumbHeight = 200;
gPhotos[32].meta = new Object();

gPhotos[33] = new Object();
gPhotos[33].filename = "5.jpg";
gPhotos[33].ImageWidth = 358;
gPhotos[33].ImageHeight = 500;
gPhotos[33].ThumbWidth = 143;
gPhotos[33].ThumbHeight = 200;
gPhotos[33].meta = new Object();

gPhotos[34] = new Object();
gPhotos[34].filename = "50.jpg";
gPhotos[34].ImageWidth = 500;
gPhotos[34].ImageHeight = 334;
gPhotos[34].ThumbWidth = 200;
gPhotos[34].ThumbHeight = 133;
gPhotos[34].meta = new Object();

gPhotos[35] = new Object();
gPhotos[35].filename = "51.jpg";
gPhotos[35].ImageWidth = 500;
gPhotos[35].ImageHeight = 334;
gPhotos[35].ThumbWidth = 200;
gPhotos[35].ThumbHeight = 133;
gPhotos[35].meta = new Object();

gPhotos[36] = new Object();
gPhotos[36].filename = "6.jpg";
gPhotos[36].ImageWidth = 500;
gPhotos[36].ImageHeight = 341;
gPhotos[36].ThumbWidth = 200;
gPhotos[36].ThumbHeight = 136;
gPhotos[36].meta = new Object();

gPhotos[37] = new Object();
gPhotos[37].filename = "7.jpg";
gPhotos[37].ImageWidth = 330;
gPhotos[37].ImageHeight = 500;
gPhotos[37].ThumbWidth = 132;
gPhotos[37].ThumbHeight = 200;
gPhotos[37].meta = new Object();

gPhotos[38] = new Object();
gPhotos[38].filename = "9.jpg";
gPhotos[38].ImageWidth = 477;
gPhotos[38].ImageHeight = 500;
gPhotos[38].ThumbWidth = 191;
gPhotos[38].ThumbHeight = 200;
gPhotos[38].meta = new Object();

